<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть более 6 символов и совпадать с подтверждением.',
    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'Ссылка для сброса пароля отправлена на Ваш email!',
    'token' => 'Неверный токе сброса пароля.',
    'user' => "Мы не можем найти пользователя с таким email.",

];

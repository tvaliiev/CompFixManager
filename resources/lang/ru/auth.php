<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Введенные данные не совпадают ни с одной из записей',
    'throttle' => 'Млишком много попыток, попробуйте через in :seconds секунд.',

];

@extends('layouts.app')

@section('content')
    @php
        $message = Request::query('m', '');
    @endphp
    @if($message != '')
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning">
                    @php
                        switch ($message){
                            case '0':
                            echo __('Your account disabled! Ask your administrator for support.');
                            break;
                            case '1':
                            echo __('You can`t work from this ip.');
                            break;
                        }
                    @endphp
                </div>
            </div>
        </div>
    @endif

    <div class="jumbotron" style="background: #333 url('/img/1.jpg') ;color: #eee;">
        <div class="row text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h1>{{config('app.name')}}</h1>
                <p>Купи слона!</p>
                <p>Купи очень много слонов!</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (Auth::guard('clients')->user())
                <a href="{{route('clients.orders')}}"
                   class="btn btn-default col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("My orders")}}</a>
                <a href="{{route('logout')}}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="btn btn-default col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("Logout")}}</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            @elseif(Auth::check())
                <a href="{{route('home')}}"
                   class="btn btn-default col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("Home")}}</a>
                <a href="{{route('logout')}}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="btn btn-default col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("Logout")}}</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            @else
                <a href="{{route('login')}}"
                   class="btn btn-default col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("Login as Manager")}}</a>
                <a href="{{route('clients.loginForm')}}"
                   class="btn btn-success col-md-12 col-sm-12 col-xs-12 m-b-1">{{__("Login as Client")}}</a>
            @endif
        </div>
    </div>
@endsection

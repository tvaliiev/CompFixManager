@extends('layouts.app')
@section('content')
    <div class="row">
        <div class=" col-xs-12 col-md-6 col-md-offset-3">
            <div class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{route('users.update', $user->id)}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        @php
                            $is_priveleged = Auth::user()->group->can_create_update_users;
                            $updatable_fields = Auth::user()->getUpdatableFields();

                            $initial_config = [
                                    'name' => ['type'=>'text', 'label'=>__('Name').':', 'value'=>$user->name],
                                    'email' => ['type'=>'email', 'label'=>__('Email').':', 'value'=>$user->email],
                                    'phone_number' => ['type'=>'text', 'label'=>__('Phone number').':', 'value'=>$user->phone_number],
                                    'group_id' => [
                                        'type'=>'select', 'label'=>__('Group').':','value_field'=>'id',
                                        'name_field'=>'name', 'empty_option'=> true,
                                        'options'=>[
                                            'values'=>$groups,
                                            'selected'=>$user->group?$user->group->id:'',
                                        ]
                                    ],
                                    'allowed_ip' => ['type'=>'text', 'label'=>__('Allowed IP to work from').':', 'value'=>$user->allowed_ip],
                                    'password' => ['type'=>'password', 'label'=>__('Type new password to change').':', 'value'=>''],
                                    'password_confirmation' => ['type'=>'password', 'label'=>__('Confirm password').':', 'value'=>''],
                                    'is_active' => ['type'=>'checkbox', 'label'=>__('Is active user?'), 'checked' => $user->is_active == "1"]
                                ];
                            $config = [];

                            if($is_priveleged == '0') {
                                foreach ($initial_config as $key => $value) {
                                    if(in_array($key, $updatable_fields)) {
                                        $config[$key] = $value;

                                        if($key == 'password')
                                          $config['password_confirmation'] = ['type'=>'password', 'label'=>__('Confirm password').':', 'value'=>''];
                                    }
                                }
                            } else{
                                $config = $initial_config;
                            }
                        @endphp
                        {!! Form::make($config) !!}
                        <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
                        <button type="submit" class="btn btn-success pull-right">{{__('Submit')}}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@extends('layouts.app')
@section('content')
    <div class="row ">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <form action="{{route('users.store')}}" method="post">
                {{csrf_field()}}
                {!! Form::make([
                    'name' => ['type'=> 'text', 'label'=>__('Name').':', 'value'=>old('name')],
                    'email' => ['type'=> 'email', 'label'=>__('Email').':', 'value'=>old('email')],
                    'group_id' => [
                        'type'=> 'select', 'label'=>__('Group').':', 'empty_option' => false,
                        'name_field' => 'name', 'value_field'=>'id',
                        'options'=> ['values' => $groups, 'selected' => $default_group_id],
                    ],
                    'password' => ['type'=> 'password', 'label'=>__('Password').':', 'value'=>old('password')],
                    'password_confirmation' => ['type'=> 'password', 'label'=>__('Password').':', 'value'=>old('password_confirmation')],
                ]) !!}
                <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
                <input type="submit" class="btn btn-success pull-right" value="{{__('Submit')}}">
            </form>
        </div>
    </div>
@stop

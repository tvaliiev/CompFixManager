@extends('layouts.app')

@section('content')
    @php
        unset($query['page']);
        $is_extended_search = count($query) >= 1;
    @endphp
    <div class="row p-b-2 search">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading mouse-pointer">
                    <h2 class="panel-title" style="display: inline; height: 100%;">
                        <i class="glyphicon glyphicon-search m1"></i>
                        {{__('Search')}}
                    </h2>
                </div>

                <div class="panel-body" {!! $is_extended_search?'':'style="display: none;"' !!}>
                    <form action="{{route('users.index')}}" method="get">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                {!! Form::input('text', 'name:like', __('Name').':', $query['name:like']??'') !!}
                            </div>
                            <div class="col-xs-12 col-md-6">
                                {!! Form::input('text', 'email:like', __('Email').':', $query['email:like']??'') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                {!! Form::select('group_id:eq', __('Group').':', $groups, $query['group_id:eq']??'', 'id', 'name', true) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn btn-default" href="{{route('users.index')}}">{{__('Clear form')}}</a>
                                <input type="submit" class="btn btn-primary" value="{{__('Search')}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('users.create')}}" class="btn btn-success">{{__('Create')}}</a>
        </div>
    </div>
    @if(count($users) > 0)
        <div class="row">
            <div class="col-xs-12">
                {{$users->links()}}
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Group')}}</th>
                            <th class="text-center">{{__('Available actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->group ? $user->group->name : '-'}}</td>
                                <td class="text-center">
                                    @can('view_update_form', $user)
                                        <a href="{{route("users.edit", $user->id)}}" class="btn btn-primary"><i
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                    @endcan
                                    @can('delete', $user)
                                        <a href="{{route("users.delete", $user->id)}}" class="btn btn-danger"><i
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$users->links()}}
            </div>
        </div>

    @else
        <div class="row">
            <div class="col-xs-12 text-center">
                {{__('No such users')}}
            </div>
        </div>
    @endif
@stop

@push('scripts')
<script>
    var extended_search = '{{$is_extended_search}}' == '1';
    $('.search .panel-heading').click(function () {
        extended_search = !extended_search;

        var $search_body = $('.search .panel-body');
        if (extended_search) {
            $search_body.show('blind');
        } else {
            $search_body.hide('blind');
        }
    });
</script>
@endpush

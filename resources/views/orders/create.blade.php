@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('orders.store')}}" method="post">
            {{csrf_field()}}
            {!! Form::make([
                'task' => ['type'=> 'text', 'label'=>__('Task').':', 'value'=>old('task')],
                'client_id' => [
                    'type' => 'select', 'label' => __('Client').':', 'name_field' => 'name', 'value_field'=>'id',
                    'options' => [
                        'values' => $clients, 'selected' => old('client_id') ?? $clients[0]->id ?? null], 'empty_option' => true,
                ],
                'device_id' => [
                    'type' => 'select', 'label' => __('Device').':', 'name_field' => 'hwid', 'value_field'=>'id',
                    'options' => [
                        'values' => $devices,
                        'selected' => (old('device_id') ?? $request->query('device_id') ?? null),
                    ],
                    'empty_option' => false,
                ],
                'comment' => ['type'=> 'textarea', 'rows' => 7, 'label'=>__('Comment').':', 'value'=>old('comment')],
            ]) !!}
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <input type="submit" class="btn btn-success pull-right" value="{{__('Submit')}}">
        </form>
    </div>
@stop
@push('scripts')
<script>
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}})

    function formatClient(client) {
        if (client.loading) return '<div>{{__('Loading')}}...</div>'
        return '<div>' + client.name + '</div>';
    }

    function formatClientSelection(client) {
        return client.text || client.name || '{{__('Loading')}}';
    }

    var $client = $("#client_id");
    $client.select2({
        ajax: {
            url: '{{route('api.clients.list')}}',
            headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'},
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    "name:like": params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * data.per_page) < data.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        templateResult: formatClient,
        templateSelection: formatClientSelection
    })
            .on("select2:select", updateDevices);

    function updateDevices() {
        var client_id  = $('#client_id').val();

        $.get('{{route('api.client.devices')}}', {client_id: client_id}, function (data) {
            $device.select2().empty();
            $device.select2({
                disabled: false,
                data: data,
            }, true);
        });
    }

    var $device = $('#device_id');
    var device_select_disabled = '{{count($devices)}}' == 0;
    console.log(device_select_disabled);
    $device.select2({
        disabled: device_select_disabled,
        data: [],
    });

    $('input[name="status"]').autocomplete({
        source: "{{route('api.statuses.list')}}",
        minLength: 2
    });
</script>
@endpush

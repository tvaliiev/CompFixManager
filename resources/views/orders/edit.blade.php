@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('orders.update', $order->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            {!! Form::make([
                'task' => ['type'=> 'text', 'label'=>__('Task').':', 'value'=>$order->task],
                'client_id' => [
                    'type' => 'select', 'label' => __('Client').':', 'name_field' => 'name', 'value_field'=>'id',
                    'options' => ['values' => $clients, 'selected' => $order->client->id], 'empty_option' => true,
                ],
                'device_id' => [
                    'type' => 'select', 'label' => __('Device').':', 'name_field' => 'hwid', 'value_field'=>'id',
                    'options' => ['values' => $devices, 'selected' => $order->device->id], 'empty_option' => false,
                ],
                'status_option_id' => [
                    'type' => 'select', 'label' => __('Status').':', 'name_field' => 'name', 'value_field'=>'id',
                    'options' => ['values' => $available_statuses, 'selected' => $order->getCurrentStatus()->option->id], 'empty_option' => false,
                ],
                'status_comment' => ['type'=> 'textarea', 'rows' => 3, 'label'=>__('Comment to status').':', 'value'=>'', 'hidden'=> true],
                'comment' => ['type'=> 'textarea', 'rows' => 7, 'label'=>__('Comment').':', 'value'=>$order->comment],
            ]) !!}
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <button type="submit" class="btn btn-success pull-right">{{__('Submit')}}</button>
            <div class="clearfix"></div>
        </form>
    </div>
@stop
@push('scripts')
<script>
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}});

    function formatClient(client) {
        if (client.loading) return '<div>{{__('Loading')}}...</div>';
        return '<div>' + client.name + '</div>';
    }

    function formatClientSelection(client) {
        return client.text || client.name || 'Loading';
    }

    var $client = $("#client_id");
    $client.select2({
        ajax: {
            url: '{{route('api.clients.list')}}',
            headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'},
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    "name:like": params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * data.per_page) < data.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        templateResult: formatClient,
        templateSelection: formatClientSelection
    })
        .on("select2:select", updateDevices);

    function updateDevices() {
        var client_id = $('#client_id').val();

        $.get('{{route('api.client.devices')}}', {client_id: client_id}, function (data) {
            $device.select2().empty();
            $device.select2({
                data: data,
            }, true);
        });
    }

    var $device = $('#device_id');
    $device.select2({
        data: [],
    });

    var $statuses_select = $('#status_option_id');
    var original_status_id = $statuses_select.val();
    var $status_comment_parent = $('#status_comment').parent();
    $statuses_select.change(function (e) {
        if (e.target.value == original_status_id) {
            $status_comment_parent.hide();
        } else {
            $status_comment_parent.show();
        }
    });
</script>
@endpush

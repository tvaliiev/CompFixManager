<div class="row m-b-1">
    <div class="col-xs-12">
        <a href="{{route('clients.orders')}}" class="btn btn-primary">Back</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title" style="display: inline; height: 100%;">{{$order->task}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        {{__('Comment')}}:
                        <p>{!! nl2br(htmlspecialchars($order->comment)) !!}</p>
                    </div>
                    <div class="col-md-6">
                        {{__('Device')}}:
                        <p>{{$order->device->hwid}} [{{$order->device->type}}]</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">{{__('Status timeline')}}</div>
            <div class="panel-body">
                <div class="row">
                    <ul class='timeline'>
                        <li class="year first">{{$order->created_at->day}}/{{$order->created_at->month}}</li>
                        @foreach($order->statuses()->orderBy('created_at')->get() as $status)
                            <li class="event {{$loop->first?'offset-first':''}}">
                                <div>{{$status->created_at}} | {{$status->option->name}}</div>
                                {!! nl2br(htmlspecialchars($status->comment)) !!}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

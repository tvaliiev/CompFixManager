<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">{{__('Your orders')}}</h2>
            </div>

            <table class="table">
                <thead>
                <tr>
                    <th>{{__('Task')}}</th>
                    <th>{{__('Device')}}</th>
                    <th>{{__('Status')}}</th>
                </tr>
                </thead>
                <tbody>
                @forelse($orders as $order)
                    <tr>
                        <td><a href="{{route('clients.order', $order->id)}}">{{$order->task}}</a></td>
                        @if($order->device)
                            <td>{{$order->device->type}} [{{$order->device->hwid}}]</td>
                        @else
                            <td>---</td>
                        @endif
                        <td>{{$order->getCurrentStatusName()}}</td>
                    </tr>
                @empty
                    <tr>
                        <td>{{__('You don`t have orders')}}</td>
                        <td></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
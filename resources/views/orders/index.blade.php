@extends('layouts.app')

@section('content')
    @php
        unset($query['page']);
        $is_extended_search = count($query) >= 1;
    @endphp
    <div class="row p-b-2 search">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading mouse-pointer">
                    <h2 class="panel-title" style="display: inline; height: 100%;">
                        <i class="glyphicon glyphicon-search m1"></i>
                        {{__('Search')}}
                    </h2>
                </div>

                <div class="panel-body" {!! $is_extended_search?'':'style="display: none;"' !!}>
                    <form action="{{route('orders.index')}}" method="get">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::input('text', 'task:like', __('Task').':', $query['task:like'] ?? '') !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::input('text', 'client_name', __('Client name').':', $query['client_name'] ?? '') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::input('text', 'device_hwid', __('Device hwid').':', $query['device_hwid'] ?? '') !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::select(
                                        'status_option_id',
                                        __('Status').':',
                                        $status_options,
                                        $query['status_option_id'] ?? '',
                                        'id',
                                        'name',
                                        true) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn btn-default" href="{{route('orders.index')}}">{{__('Clear form')}}</a>
                                <input type="submit" class="btn btn-primary" value="{{__('Search')}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can('create', \App\Order::class)
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('orders.create')}}" class="btn btn-success">{{__('Create')}}</a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-xs-12">
            {{$orders->links()}}
            @component('orders.table', ['orders' => $orders, 'showClient' => 1, 'showDevice' => 1])
            @endcomponent
            {{$orders->links()}}
        </div>
    </div>
@stop

@push('scripts')
<script>
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}})

    $('input[name="client_name"]').autocomplete({
        source: "{{route('api.clients.names')}}",
        minLength: 2
    });

    $('input[name="device_hwid"]').autocomplete({
        source: "{{route('api.devices.hwids')}}",
        minLength: 2
    });
</script>
@endpush

@push('scripts')
<script>
    var extended_search = '{{$is_extended_search}}' == '1';
    $('.search .panel-heading').click(function () {
        extended_search = !extended_search;

        var $search_body = $('.search .panel-body');
        if (extended_search) {
            $search_body.show('blind');
        } else {
            $search_body.hide('blind');
        }
    });
</script>
@endpush
@extends('layouts.app')

@section('content')
    <div class="row m-b-1">
        <div class="col-xs-12 col-md-6">
            <div id="chart-clients-per-month" style="border: 2px solid #d0d0d0;"></div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div id="chart-orders-per-month" style="border: 2px solid #d0d0d0;"></div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ url('js/highcharts.js') }}"></script>
<script>
    Highcharts.setOptions({
        lang: {
            downloadJPEG: "{{__('Download JPEG image')}}",
            downloadPDF: "{{__('Download PDF document')}}",
            downloadPNG: "{{__('Download PNG image')}}",
            downloadSVG: "{{__('Download SVG vector image')}}",
            printChart: "{{__('Print chart')}}",
        }
    });
    Highcharts.chart('chart-clients-per-month', {
        title: {text: '{{__('New clients per month')}}'},
        xAxis: {
            categories: JSON.parse('{!! json_encode($monthClientsLabels) !!}'),
            title: {text: '{{__('Year/Month')}}'}
        },
        yAxis: {
            title: {text: '{{__('Number of clients')}}'}
        },
        legend: {enabled: false},
        series: [{
            name: '{{__('Number of clients')}}',
            data: JSON.parse('{!! json_encode($monthClientsStat) !!}')
        }]

    });
    Highcharts.chart('chart-orders-per-month', {
        title: {text: '{{__("New orders per month")}}'},
        xAxis: {
            categories: JSON.parse('{!! json_encode($monthOrdersLabels) !!}'),
            title: {text: '{{__('Year/Month')}}'}
        },
        yAxis: {
            title: {text: '{{__('Number of orders')}}'},
        },
        legend: {enabled: false},
        series: [{
            name: '{{__('Number of orders')}}',
            data: JSON.parse('{!! json_encode($monthOrdersStat) !!}')
        }]
    });
</script>
@endpush

<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>{{__('HWID')}}</th>
            <th>{{__('Type')}}</th>
            @if($showClient ?? false)
                <th>{{__('Client')}}</th>
            @endif
            <th class="text-center">{{__('Available actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @forelse($devices as $device)
            <tr>
                <td><a href="{{route('devices.show', $device->id)}}">{{$device->hwid}}</a></td>
                <td>{{$device->type}}</td>
                @if($showClient ?? false)
                    @if($device->client)
                        <td><a href="{{route('clients.show', $device->client->id)}}">{{$device->client->name}}</a></td>
                    @else
                        <td>---</td>
                    @endif
                @endif
                <td class="text-center">
                    @can('update', \App\Device::class)
                        <a href="{{route("devices.edit", $device->id)}}" class="btn btn-primary"><i
                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                    @endcan
                    @can('delete', \App\Device::class)
                        <a href="{{route("devices.delete", $device->id)}}" class="btn btn-danger"><i
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
                    @endcan
                </td>
            </tr>
        @empty
            <tr>
                <td>{{__('No such devices')}}</td>
                <td></td>
                @if($showClient ?? false)
                    <td></td>
                @endif
                <td></td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('devices.store')}}" method="post">
            {{csrf_field()}}
            {!! Form::make([
                'client_id' => [
                    'type'=> 'select', 'label'=>__('Client').':', 'empty_option' => true,
                    'name_field' => 'name', 'value_field'=>'id',
                    'options'=> ['values' => $clients ?? [], 'selected' => old('client_id') ?? $clients[0]->id ?? null]
                ],
                'hwid' => ['type' => 'text', 'label' => __('HWID').':', 'value'=>old('hwid')],
                'type' => ['type' => 'text', 'label' => __('Type').':', 'value'=>old('type')],
                'description' => ['type' => 'textarea', 'label' => __('Description').':', 'value'=>old('description'), 'rows'=>6],
            ]) !!}
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <input type="submit" class="btn btn-success pull-right" value="{{__('Submit')}}">
        </form>
    </div>
@stop
@push('scripts')
<script>
    function formatClient(client) {
        if (client.loading) return '<div>{{__('Loading')}}...</div>'
        return '<div>' + client.name + '</div>';
    }

    function formatRepoSelection(client) {
        return client.text || client.name || '{{__('Loading')}}';
    }

    $("#client_id").select2({
        ajax: {
            url: '{{route('api.clients.list')}}',
            headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'},
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    "name:like": params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * data.per_page) < data.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        templateResult: formatClient,
        templateSelection: formatRepoSelection
    });
</script>
@endpush
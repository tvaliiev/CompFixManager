<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">{{$heading ?? __('Devices').':'}}</h2>
            </div>

            @component('devices.table', ['devices' => $devices])
            @endcomponent
        </div>
    </div>
</div>
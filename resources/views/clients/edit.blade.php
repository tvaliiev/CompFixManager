@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('clients.update', $client->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            {!! Form::make([
                'name' => ['type'=> 'text', 'label'=>__('Name').':', 'value'=>$client->name],
                'email' => ['type'=> 'text', 'label'=>__('Email').':', 'value'=>$client->email],
                'phone' => ['type'=> 'text', 'label'=>__('Phone').':','value'=>$client->phone],
                'comment' => ['type'=> 'textarea', 'rows' => 7, 'label'=>__('Comment').':', 'value'=>$client->comment],
            ]) !!}
            <hr>
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <button type="submit" class="btn btn-success pull-right">{{__('Submit')}}</button>
            <div class="clearfix"></div>
        </form>
    </div>
@stop
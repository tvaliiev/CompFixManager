@extends('layouts.client')

@section('content')
    @component('orders.client-orders', [
        'heading' =>__('Your orders'),
        'orders' => $orders,
    ])
    @endcomponent
@stop
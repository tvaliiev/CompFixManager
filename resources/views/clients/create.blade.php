@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('clients.store')}}" method="post">
            {{csrf_field()}}
            {!! Form::make([
                'name' => ['type'=> 'text', 'label'=>__('Name').':', 'value'=>old('name')],
                'email' => ['type'=> 'text', 'label'=>__('Email').':', 'value'=>old('email')],
                'phone' => ['type'=> 'text', 'label'=>__('Phone').':', 'value'=>old('phone')],
                'comment' => ['type'=> 'textarea', 'rows' => 7, 'label'=>__('Comment').':', 'value'=>old('comment')],
            ]) !!}
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <input type="submit" class="btn btn-success pull-right" value="{{__('Submit')}}">
        </form>
    </div>
@stop
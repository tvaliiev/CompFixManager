@extends('layouts.client')
@section('content')
    @component('orders.client-order', ['order' => $order])
    @endcomponent
@stop

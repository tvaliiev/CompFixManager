@extends('layouts.app')

@section('content')
    @php
        unset($query['page']);
        $is_extended_search = count($query) >= 1;
    @endphp
    <div class="row p-b-1 search">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading mouse-pointer">
                    <h2 class="panel-title" style="display: inline; height: 100%;">
                        <i class="glyphicon glyphicon-search m1"></i>
                        {{__('Search')}}
                    </h2>
                </div>

                <div class="panel-body" {!! $is_extended_search?'':'style="display: none;"' !!}>
                    <form action="{{route('clients.index')}}" method="get">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::input('text', 'name:like', __('Name').':', $query['name:like']??'') !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::input('text', 'phone:like', __('Phone').':', $query['phone:like']??'') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn btn-default" href="{{route('clients.index')}}">{{__('Clear form')}}</a>
                                <input type="submit" class="btn btn-primary" value="{{__('Search')}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can('create', \App\Client::class)
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('clients.create')}}" class="btn btn-success">{{__('Create')}}</a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-xs-12">
            {{$clients->links()}}
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{__('Name')}}</th>
                        <th>{{__('Email')}}</th>
                        <th>{{__('Phone')}}</th>
                        <th class="text-center">{{__('Available actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($clients as $client)
                        <tr>
                            <td><a href="{{route('clients.show', $client->id)}}">{{$client->name}}</a></td>
                            <td>{{$client->email}}</td>
                            <td>{{$client->phone}}</td>
                            <td class="text-center">
                                @can('update', \App\Client::class)
                                    <a href="{{route("clients.edit", $client->id)}}" class="btn btn-primary"><i
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                @endcan
                                @can('delete', \App\Client::class)
                                    <a href="{{route("clients.delete", $client->id)}}" class="btn btn-danger"><i
                                                class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>{{__('No such clients')}}</td>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            {{$clients->links()}}
        </div>
    </div>

@stop

@push('scripts')
<script>
    var extended_search = '{{$is_extended_search}}' == '1';
    $('.search .panel-heading').click(function () {
        extended_search = !extended_search;

        var $search_body = $('.search .panel-body');
        if (extended_search) {
            $search_body.show('blind');
        } else {
            $search_body.hide('blind');
        }
    });
</script>
@endpush

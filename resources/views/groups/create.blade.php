@extends('layouts.app')
@section('content')
    <div class="row col-xs-12 col-md-6 col-md-offset-3">
        <form action="{{route('groups.store')}}" method="post">
            {{csrf_field()}}
            {!! Form::make([
                'name' => ['type'=> 'text', 'label' => __('Name').':', 'value'=> old('name')]
            ]) !!}
            <hr>
            <h3>{{__('Group rights')}}:</h3>
            <table class="table text-center">
                <thead>
                <tr>
                    <td></td>
                    <td>{{__('View list')}}</td>
                    <td>{{__('Create or update')}}</td>
                    <td>{{__('Delete')}}</td>
                </tr>
                </thead>
                <tbody>
                @foreach([
                    'users' => __('Users'),
                    'groups' => __('Groups'),
                    'orders' => __('Orders'),
                    'clients' => __('Clients'),
                    'devices' => __('Devices'),
                ] as $key => $name)
                    <tr>
                        <td style="vertical-align: middle;">{{$name}}</td>
                        <td>{!! Form::checkbox("can_view_$key") !!}</td>
                        <td>{!! Form::checkbox("can_create_update_$key") !!}</td>
                        <td>{!! Form::checkbox("can_delete_$key") !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
            <div class="btn btn-default" onclick="window.history.back()">{{__('Back')}}</div>
            <button type="submit" class="btn btn-success pull-right">{{__('Submit')}}</button>
            <div class="clearfix"></div>
        </form>
    </div>
@stop
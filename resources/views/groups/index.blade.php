@extends('layouts.app')

@section('content')
    @can('create', \App\Group::class)
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('groups.create')}}" class="btn btn-success">{{__('Create')}}</a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-xs-12">
            {{$groups->links()}}
            <table class="table">
                <thead>
                <tr>
                    <th>{{__('Name')}}</th>
                    <th class="text-center">{{__('Available actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @forelse($groups as $group)
                    <tr>
                        <td><a href="{{route("groups.show", $group->id)}}">{{$group->name}}</a></td>
                        <td class="text-center">
                            @can('update', \App\Group::class)
                                <a href="{{route("groups.edit", $group->id)}}" class="btn btn-primary"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                            @endcan
                            @can('delete', \App\Group::class)
                                <a href="{{route("groups.delete", $group->id)}}" class="btn btn-danger"><i
                                            class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
                            @endcan
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td conspan="2">{{__('No such groups')}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{$groups->links()}}
        </div>
    </div>

@stop
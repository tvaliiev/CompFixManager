<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{route('welcome')}}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            @if(!Auth::guest())
                <ul class="nav navbar-nav">
                    <li class="nav-item"><a href="{{route('home')}}"><span class="glyphicon glyphicon-home"></span></a>
                    </li>
                    @if(Auth::user()->can('view', \App\User::class) ||
                    Auth::user()->can('view', \App\Group::class) ||
                    Auth::user()->can('update', \App\SystemSetting::class))
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">{{__('Admin')}} <span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @can('view', \App\User::class)
                                    <li>
                                        <a href="{{ route('users.index') }}"
                                           class="{{Request::is('*/users*')?'active':''}}">
                                            {{__('Users')}}
                                        </a>
                                    </li>
                                @endcan
                                @can('view', \App\Group::class)
                                    <li>
                                        <a href="{{ route('groups.index') }}"
                                           class="{{Request::is('*/groups*')?'active':''}}">
                                            {{__('Groups')}}
                                        </a>
                                    </li>
                                @endcan
                                @can('update', \App\SystemSetting::class)
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{ route('system_settings') }}"
                                           class="{{Request::is('*/system_settings*')?'active':''}}">
                                            {{__('System settings')}}
                                        </a>
                                    </li>
                                @endcan
                                <li>
                                    <a href="{{ route('statistic') }}"
                                       class="{{Request::is('*/statistic*')?'active':''}}">
                                        {{__('Statistic')}}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @can('view', \App\Order::class)
                        <li class="nav-item">
                            <a href="{{route('orders.index')}}" class="{{Request::is('*/orders*')?'active':''}}">
                                {{__('Orders')}}
                            </a>
                        </li>

                        @can('create', \App\Order::class)
                            <li class="nav-plus">
                                <a href="{{route('orders.create')}}" style="padding-right: 0 !important;">
                                    <i class="glyphicon glyphicon-plus hidden-xs"></i>
                                    <span class="visible-xs-inline">{{__('Create order')}}</span>
                                </a>
                            </li>
                            <li class="nav-plus">
                                <a href="{{route('orders.create2')}}">
                                    <i class="glyphicon glyphicon-plus hidden-xs" style="color:#6584ff;"></i>
                                    <span class="visible-xs-inline">
                                        {{__('Create order (smart)')}}
                                    </span>
                                </a>
                            </li>
                        @endcan
                    @endcan
                    @can('view', \App\Client::class)
                        <li class="nav-item">
                            <a href="{{route('clients.index')}}" class="{{Request::is('*/clients*')?'active':''}}">
                                {{__('Clients')}}
                            </a>
                        </li>
                        <li class="nav-plus">
                            <a href="{{route('clients.create')}}">
                                <i class="glyphicon glyphicon-plus hidden-xs"></i>
                                <span class="visible-xs-inline">{{__('Create client')}}</span>
                            </a>
                        </li>
                    @endcan
                    @can('view', \App\Device::class)
                        <li class="nav-item">
                            <a href="{{route('devices.index')}}" class="{{Request::is('*/devices*')?'active':''}}">
                                {{__('Devices')}}
                            </a>
                        </li>
                        <li class="nav-plus">
                            <a href="{{route('devices.create')}}">
                                <i class="glyphicon glyphicon-plus hidden-xs"></i>
                                <span class="visible-xs-inline">{{__('Create device')}}</span>
                            </a>
                        </li>
                    @endcan
                </ul>
        @endif

        <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @include('layouts.components.language-selector')
                @include('layouts.components.theme-selector')
                @if (Auth::user())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('users.edit', Auth::user()->id)}}">{{__('Settings')}}</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{__('Logout')}}

                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
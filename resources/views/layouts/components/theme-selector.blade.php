<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">
        <i class="glyphicon glyphicon-tint"></i>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        @foreach(config('app.themes') as $theme)
            <li>
                <a href="{{route('theme', $theme)}}">
                    <span>{{__(config("app.theme_strings.$theme"))}}</span>
                    @if(config('app.theme') == $theme)
                        <i class="glyphicon glyphicon-ok"></i>
                    @endif
                </a>
            </li>
        @endforeach
    </ul>
</li>
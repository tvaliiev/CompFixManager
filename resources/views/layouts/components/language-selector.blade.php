<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">
        <img src="{{url("language-flags/".App::getLocale().".png")}}" alt="{{App::getLocale()}} icon"> <span
                class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        @foreach(config('app.locales') as $language)
            <li>
                <a href="{{route('language', $language)}}">
                    <img src="{{url("language-flags/$language.png")}}"
                         alt="{{$language}} icon">
                    {{config("app.locales_translations.$language")}}
                </a>
            </li>
        @endforeach
    </ul>
</li>



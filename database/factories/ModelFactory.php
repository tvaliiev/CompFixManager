<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => 'password',
        'remember_token' => '',
        'phone_number' => $faker->phoneNumber,
        'api_token' => str_random(60),

    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    $name = $faker->name;
    $t = $faker->dateTimeBetween('-1 years');

    return [
        'name' => $name,
        'comment' => join(PHP_EOL, $faker->paragraphs(2)),
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'password' => 'password',
        'created_at' => $t,
        'updated_at' => $t,
    ];
});

$factory->define(App\Device::class, function (Faker\Generator $faker, $additional_fields) {
    $client = \App\Client::find($additional_fields['client_id']);

    $r_i = random_int(0, 5);
    $d_c = $client->created_at->addWeeks($r_i);
    $d_u = $client->updated_at->addWeeks($r_i);
    return [
        'hwid' => "{$faker->randomNumber(7)}",
        'description' => join(PHP_EOL, $faker->paragraphs(2)),
        'type' => $faker->randomElement(['Ноутбук', 'Телефон', 'Компьютер', 'Мышка', 'Клавиатура']),
        'created_at' => $d_c,
        'updated_at' => $d_u,
    ];
});

$factory->define(App\Order::class, function (Faker\Generator $faker, $additional_fields) {
    $device = \App\Device::find($additional_fields['device_id']);

    $r_i = random_int(0, 2);
    $d_c = $device->created_at->addWeeks($r_i);
    $d_u = $device->updated_at->addWeeks($r_i);
    return [
        'task' => join(' ', $faker->words(random_int(3, 5))),
        'comment' => join(PHP_EOL, $faker->paragraphs(2)),
        'created_at' => $d_c,
        'updated_at' => $d_u,
    ];
});

$factory->define(App\Status::class, function (Faker\Generator $faker){
    return [
        'comment' => join(' ', $faker->words(random_int(1, 3))),
    ];
});
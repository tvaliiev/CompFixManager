<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceRights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->boolean('can_view_devices')->default(0);
            $table->boolean('can_create_update_devices')->default(0);
            $table->boolean('can_delete_devices')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->removeColumn('can_view_devices');
            $table->removeColumn('can_create_update_devices');
            $table->removeColumn('can_delete_devices');
        });
    }
}

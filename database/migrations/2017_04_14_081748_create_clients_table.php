<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->text('comment');
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->boolean('can_view_clients')->default(0);
            $table->boolean('can_create_update_clients')->default(0);
            $table->boolean('can_delete_clients')->default(0);
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->nullable();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('set null');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->nullable();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->removeColumn('client_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->removeColumn('client_id');
        });

        Schema::dropIfExists('clients');

        Schema::table('groups', function (Blueprint $table) {
            $table->removeColumn('can_view_clients');
            $table->removeColumn('can_create_update_clients');
            $table->removeColumn('can_delete_clients');
        });
    }
}

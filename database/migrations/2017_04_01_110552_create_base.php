<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('changed_id')->unsigned()->nullable();
            $table->integer('order_id')->unsigned();
            $table->text('comment');
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('created_id')->unsigned()->nullable();
            $table->integer('device_id')->unsigned()->nullable();
            $table->string('task');
            $table->text('comment');
        });

        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type');
            $table->string('hwid');
            $table->text('description');

        });

        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('statuses', function (Blueprint $table) {
            $table
                ->foreign('changed_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table
                ->foreign('created_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table
                ->foreign('device_id')
                ->references('id')
                ->on('devices')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('devices');
    }
}

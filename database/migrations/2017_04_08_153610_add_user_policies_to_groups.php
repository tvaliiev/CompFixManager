<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserPoliciesToGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->boolean('can_view_users')->default(0)->nullable();
            $table->boolean('can_create_update_users')->default(0)->nullable();
            $table->boolean('can_delete_users')->default(0)->nullable();
            $table->boolean('can_view_groups')->default(0)->nullable();
            $table->boolean('can_delete_groups')->default(0)->nullable();
            $table->boolean('can_create_update_groups')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            $table->removeColumn('can_view_users');
            $table->removeColumn('can_create_update_users');
            $table->removeColumn('can_delete_users');
            $table->removeColumn('can_view_groups');
            $table->removeColumn('can_delete_groups');
            $table->removeColumn('can_create_update_groups');
        });
    }
}

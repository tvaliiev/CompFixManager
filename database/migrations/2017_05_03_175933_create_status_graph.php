<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusGraph extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('status_graph', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('from_id')->unsigned()->nullable();
            $table
                ->foreign('from_id')
                ->references('id')
                ->on('status_options')
                ->onDelete('cascade');

            $table->integer('to_id')->unsigned()->nullable();
            $table
                ->foreign('to_id')
                ->references('id')
                ->on('status_options')
                ->onDelete('cascade');
        });

        Schema::table('statuses', function (Blueprint $table) {
            $table->integer('status_option_id')->unsigned()->nullable();

            $table
                ->foreign('status_option_id')
                ->references('id')
                ->on('status_options')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_graph');
        Schema::table('statuses', function(Blueprint $table){
           $table->dropForeign(['status_option_id']);
            $table->removeColumn('status_option_id');
        });
        Schema::dropIfExists('status_options');
    }
}

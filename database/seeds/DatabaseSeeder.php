<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();
        $output->writeln('Starting to populate database with demo data...');

        $output->writeln('Creating user group...');
        $user_group = \App\Group::create([
            'name' => 'User',
        ]);
        $output->writeln('Done.');

        $output->writeln('Creating users...');
        factory(\App\User::class, 5)->create(['group_id' => $user_group->id]);
        $output->writeln('Done.');

        $output->writeln('Creating some statuses and basic graph...');
        $status_created = \App\StatusOption::first();
        $status_finished = \App\StatusOption::create(['name' => 'Finished']);
        \App\StatusGraph::create(['from_id' => $status_created->id, 'to_id' => $status_finished->id]);
        $output->writeln('Done.');

        $output->writeln('Creating clients with devices and orders...');
        $admin = \App\User::first();
        factory(\App\Client::class, random_int(47, 57))
            ->create()
            ->each(function ($c) use ($admin, $status_created) {
                factory(\App\Device::class, random_int(2, 5))
                    ->create([
                        'client_id' => $c->id,
                    ])
                    ->random(2)
                    ->each(function ($d) use ($c, $admin, $status_created) {
                        factory(\App\Order::class, random_int(0, 3))->create([
                            'client_id' => $c->id,
                            'device_id' => $d->id,
                            'created_id' => $admin->id
                        ])->each(function ($o) use ($admin, $status_created) {
                            factory(\App\Status::class)->create(['changed_id' => $admin->id, 'order_id' => $o->id, 'status_option_id' => $status_created->id]);
                        });
                    });
            });
        $output->writeln('Done.');

    }
}

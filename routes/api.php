<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/clients', ['as' => 'api.clients.list', 'uses' => 'ClientController@getClientsList'])
        ->middleware('can:view,App\Client');
    Route::get('/clients_names', ['as' => 'api.clients.names', 'uses' => 'ClientController@getClientNamesLike'])
        ->middleware('can:view,App\Client');
    Route::get('/client_devices', ['as' => 'api.client.devices', 'uses' =>'ClientController@getClientDevices'])
        ->middleware('can:view,App\Device');

    Route::get('/devices_hwids', ['as' => 'api.devices.hwids', 'uses' => 'DeviceController@getDevicesHwidsLike'])
        ->middleware('can:view,App\Device');
    Route::get('/devices', ['as' => 'api.devices.list', 'uses' => 'DeviceController@getDevicesLike'])
        ->middleware('can:view,App\Device');

    Route::get('/statuses', ['as' => 'api.statuses.list', 'uses' => 'StatusController@getAll'])
    ->middleware('can:view,App\Order');
});

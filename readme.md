### Первый запуск
Краткий гайд по первичной настройке и запуску приложения на Linux:
```
$ compose install
$ chmod 777 storage bootstrap/cache
$ cp .env{.example,}
$ docker-compose up -d
$ docker exec compfixmanager_webserver_1 php artisan key:generate
```

Это все! Сервер доступен по адресу  http://localhost:8080/

### Последующий запуск
В последующем запускать еще проще:
```
$ docker-compose up # а так же ключ "-d" если в бэкграунде
```

### Остановка контейнеров
Если запуск происходил с ключом "-d":
```
$ docker-compse down
```
Если без, то достаточно нажать Crtl+C и подождать завершения процессов.
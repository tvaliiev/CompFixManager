<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SystemSettingsPolicy
{
    use HandlesAuthorization;

    public function update(User $user)
    {
        return $user->group->can_update_system_settings == "1";
    }
}

<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Определяет, может ли пользователь просматривать список заказов.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function view(User $user)
    {
        return $user->group->can_view_clients == "1";
    }

    /**
     * Проверяет, может ли пользователь создавать заказы.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->group->can_create_update_clients == "1";
    }

    /**
     * Проверяем, может ли пользователь удалить заказ.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->group->can_create_update_clients == "1";
    }

    /**
     * Проверяет, может ли пользователь удалить заказ.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->group->can_delete_clients == "1";
    }
}

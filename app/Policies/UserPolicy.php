<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

/**
 * Класс UserPolicy
 * @package App\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Может ли пользователь просмотривать пользователей.
     *
     * @param  \App\User $current_user
     * @return boolean
     */
    public function view(User $current_user)
    {
        return $current_user->group->can_view_users == 1;
    }

    /**
     * Может ли пользователь создавать других пользователей.
     *
     * @param  \App\User $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->group->can_create_update_users == 1;
    }

    /**
     * @param User $current_user
     * @param User $user
     * @return bool
     */
    public function view_update_form(User $current_user, User $user)
    {
        if ($current_user->group->can_create_update_users == 1)
            return true;

        if ($user->id == $current_user->id)
            return true;

        return false;
    }

    /**
     * @param User $current_user
     * @param Request $request
     * @return bool
     */
    public function update(User $current_user, User $user, $request_data)
    {
        if ($current_user->group->can_create_update_users == 1)
            return true;

        // Если текущий пользователь изменяет сам себя.
        // Проверяем, не изменено ли поле, но означенное в updatable.
        foreach ($request_data as $key => $val) {
            if (in_array($key, $current_user->getUpdatableFields()))
                continue;

            if (!empty($user[$key]) && $request_data[$key] != $user[$key]) {
                dd($key, $request_data, $user);
                return false;
            }

        }

        return true;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User $user
     * @param  \App\User $user
     * @return boolean
     */
    public function delete(User $current_user, User $user)
    {
        return $current_user->group->can_delete_users == 1;
    }
}

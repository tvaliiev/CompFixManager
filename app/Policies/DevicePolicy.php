<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DevicePolicy
{
    use HandlesAuthorization;

    /**
     * Определяет, может ли пользователь просматривать список всех девайсов в системе.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function view(User $user)
    {
        return $user->group->can_view_devices == "1";
    }

    /**
     * Проверяет, может ли пользователь создавать устройства.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->group->can_create_update_devices == "1";
    }

    /**
     * Проверяем, может ли пользователь удалить устройство.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function update(User $user)
    {
        return $user->group->can_create_update_devices == "1";
    }

    /**
     * Проверяет, может ли пользователь удалить устройство.
     *
     * @param  \App\User  $user
     * @return boolean
     */
    public function delete(User $user)
    {
        return $user->group->can_delete_clients == "1";
    }
}

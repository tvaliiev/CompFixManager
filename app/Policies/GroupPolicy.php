<?php

namespace App\Policies;

use App\User;
use App\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Определяет, может ли пользователь просматривать список групп,
     * или сами группы
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->group->can_view_groups == 1;
    }

    /**
     * Определяет, может ли пользователь создавать группы.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->group->can_create_update_groups == 1;
    }

    /**
     * Определяет, может ли пользователь апдейтить группу.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->group->can_create_update_groups == 1;
    }

    /**
     * Определяет, может ли пользователь удалять группу.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->group->can_delete_groups == 1;
    }
}

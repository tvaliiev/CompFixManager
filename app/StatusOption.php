<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StatusOption
 * @package App
 */
class StatusOption extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];
}

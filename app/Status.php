<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Класс Статус.
 *
 * @package App
 * @property integer id Идентификатор статуса.
 * @property Carbon created_at Дата создания статуса.
 * @property Carbon updated_at Дата последнего изменения любого из атрибутов статуса.
 * @property integer changed_id Идентификатор пользователя, создавшего данный статус.
 * @property integer order_id Идентификатор заказа, которому принадлежит данный статус.
 * @property string name Название статуса.
 * @property string comment Комментарий к статусу.
 */
class Status extends Model
{
    /**
     * Используемая таблица в базе данных для модели.
     *
     * @var string
     */
    protected $table = 'statuses';

    /**
     * Массово присваеваемые поля.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'changed_id', 'order_id', 'status_option_id'
    ];

    /**
     * Заказ, которому принадлежит данный статус.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * Пользователь, создавший данный статус.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function user() {
        return $this->belongsTo(User::class, 'changed_id');
    }

    /**
     * Опшн статуса.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function option() {
        return $this->belongsTo(StatusOption::class, 'status_option_id');
    }
}

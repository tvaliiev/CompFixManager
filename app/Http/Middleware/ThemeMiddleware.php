<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class ThemeMiddleware
{
    /**
     * Set theme
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $theme = $request->cookie('theme');

        if (!$theme || !in_array($theme, config('app.themes'))) {
            $theme = config('app.default_theme');
            Cookie::queue('theme', $theme);
        }

        config(['app.theme' => $theme]);

        return $next($request);
    }
}

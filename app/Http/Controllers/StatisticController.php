<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    function index(Request $request)
    {

        $monthClientsStatTmp = $this->statisticCountByMonth('clients', 'created_at')->get();
        $monthClientsStat = [];
        $monthClientsLabels = [];

        foreach ($monthClientsStatTmp as $stat) {
            $monthClientsStat = array_prepend($monthClientsStat, $stat->count);
            $monthClientsLabels = array_prepend($monthClientsLabels, "$stat->year/$stat->month");
        }

        $monthOrdersStatTmp = $this->statisticCountByMonth('orders', 'created_at')->get();
        $monthOrdersStat = [];
        $monthOrdersLabels = [];

        foreach ($monthOrdersStatTmp as $stat) {
            $monthOrdersStat = array_prepend($monthOrdersStat, $stat->count);
            $monthOrdersLabels = array_prepend($monthOrdersLabels, "$stat->year/$stat->month");
        }


        return view('statistic.index', compact('monthClientsStat', 'monthClientsLabels', 'monthOrdersStat', 'monthOrdersLabels'));
    }

    private function statisticCountByMonth($table, $date_column)
    {
        $queryUsers = DB::table($table)
            ->select(DB::raw('COUNT(*) as count'),
                DB::raw('YEAR(`' . $date_column . '`) AS year'),
                DB::raw('MONTH(`' . $date_column . '`) AS month'))
            ->groupBy(DB::raw('YEAR(`' . $date_column . '`)'), DB::raw('MONTH(`' . $date_column . '`)'))
            ->orderBy(DB::raw('YEAR(`' . $date_column . '`)'), DB::raw('MONTH(`' . $date_column . '`)'));
        // We returns query mo make ability to filter results, for example by datetime range
        return $queryUsers;
    }
}

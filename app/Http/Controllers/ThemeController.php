<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cookie;

class ThemeController extends Controller
{
    public function changeTheme($theme = 'default')
    {
        if (in_array($theme, config('app.themes'))) {
            Cookie::queue('theme', $theme);
            config(['app.theme' => $theme]);
        }

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class ClientLoginController
 * @package App\Http\Controllers\Auth
 */
class ClientLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * ClientLoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Показ формы логина для клиента.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('clients.auth.login');
    }

    /**
     * Where to redirect users after login.
     */
    protected function redirectTo()
    {
        return route('clients.orders');
    }

    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('clients');
    }
}

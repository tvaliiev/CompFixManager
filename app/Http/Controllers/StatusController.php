<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function getAll(Request $request)
    {
        $term = $request->query('term');

        return Status::select('name', 'id')->where('name', 'like', "%$term%")->get();
    }
}

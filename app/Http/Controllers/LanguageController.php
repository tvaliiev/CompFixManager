<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Cookie;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class LanguageController extends Controller
{

    /**
     * Изменение языка.
     *
     * @param string $language Строка, которая определяет, на какой язык поменять.
     * @param Request $request
     * @return Redirector
     */
    function changeLanguage($language = 'en')
    {
        if (in_array($language, config('app.locales'))) {
            Cookie::queue('locale', $language);
        }

        return redirect()->back();
    }
}

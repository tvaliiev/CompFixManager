<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Traits\Filter;

/**
 * Класс ClientController
 * @package App\Http\Controllers
 */
class ClientController extends Controller
{
    use Filter;
    use AuthenticatesUsers;

    function redirectTo()
    {
        return route('clients.orders');
    }

    /**
     * Возвращает пагинированный список клиентов.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getClientsList(Request $request)
    {
        $this->authorize('view', Client::class);

        $clients = Client::select('id', 'name', 'phone', 'email');
        return $this->filter($clients, $request->query(), 'or')->paginate(Client::$apiPerPage);
    }

    /**
     * Возвращает список имен клиентов, подходящих по шаблону.
     *
     * @param Request $request
     * @return array
     */
    public function getClientNamesLike(Request $request)
    {
        $v = $request->query('term');
        $clients = Client::select('name')->where('name', 'like', "%$v%")->limit(100);

        $result = [];
        foreach ($clients->get() as $client) {
            $result[] = $client->name;
        }

        return $result;
    }

    public function getClientDevices(Request $request)
    {
        $client_id = $request->query('client_id');
        return Client::find($client_id)->devices()->select('id', 'hwid as text')->get();
    }

    /**
     * Просмотр всех клиентов (с фильтрацией).
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->authorize('view', Client::class);

        $query = $request->query();
        $clients = $this->filter(Client::query(), $query);
        $clients = $clients->paginate();
        $clients = $clients->appends($request->except('page'));
        return view('clients.index', compact('clients', 'query'));
    }

    /**
     * Просмотр формы для создание нового клиента.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', Client::class);

        return view('clients.create');
    }

    /**
     * Создание нового клиента.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', Client::class);

        $data = $request->all();
        if (is_null($data['comment'])) $data['comment'] = '';
        $data['password'] = str_random(8);

        $client = Client::create($data);

        Mail::send('mails.client_account_created', compact('data'), function ($m) use ($client) {
            $m->from('hello@compfix.com', 'CompFix');
            $m->to($client->email, $client->name)->subject('Your new account');
        });

        return redirect(route('clients.show', $client->id));
    }

    /**
     * Просмотр клиента.
     *
     * @param Client $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Client $client)
    {
        $this->authorize('view', Client::class);

        return view('clients.show', compact('client'));
    }

    /**
     * Просмотр формы редактирования клиента.
     *
     * @param Client $client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Client $client)
    {
        $this->authorize('update', Client::class);

        return view('clients.edit', compact('client'));
    }

    /**
     * Изменение существующего клиента.
     *
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Client $client)
    {
        $this->authorize('update', Client::class);

        $data = $request->all();
        if (is_null($data['comment'])) $data['comment'] = '';
        $client->fill($data);
        $client->save();

        return redirect(route('clients.show', $client->id));
    }

    /**
     * Полное удаление клиента.
     *
     * @param Client $client
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Client $client, Request $request)
    {
        $this->authorize('delete', Client::class);
        $client->delete();

        $redirect_url = $request->query('redirect_uri');
        if ($redirect_url)
            return redirect($redirect_url);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function client_orders(Request $request)
    {
        // MUST be logged in as client
        if (!is_a(Auth::user(), Client::class)) {
            return redirect(route('logout'));
        }

        $orders = Auth::user()->orders;

        return view('clients.orders', compact('orders'));
    }

    public function client_order(Order $order)
    {
        // MUST be logged in as client
        if (!is_a(Auth::user(), Client::class)) {
            return redirect(route('logout'));
        }

        // Check if client have such order
        $true_order = Auth::user()->orders->where('id', $order->id);
        if (count($true_order) == 0)
            return redirect(route('clients.orders'));

        return view('clients.order', compact('order'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Client;
use App\Device;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Traits\Filter;

/**
 * Class DeviceController
 * @package App\Http\Controllers
 */
class DeviceController extends Controller
{
    use Filter;

    /**
     * Возвращает пагинированный список устройств.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getDevicecsList(Request $request)
    {
        $devices = Device::select('id', 'hwid');
        return $this->filter($devices, $request->query())->paginate(Device::$apiPerPage)->appends($request->except('page'));
    }

    /**
     * Возвращает массив hwid устройств, найденных по совпадению.
     *
     * @param Request $request
     * @return array
     */
    public function getDevicesHwidsLike(Request $request)
    {
        $v = $request->query('term');
        $devices = Device::select('hwid')->where('hwid', 'like', "%$v%")->limit(100);

        $result = [];
        foreach ($devices->get() as $device) {
            $result[] = $device->hwid;
        }

        return $result;
    }

    /**
     * Показ всех устройств.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Device::class);

        $query = $request->query();
        $devices = $this->filter(Device::query(), $query);

        if (isset($query['client_name'])) {
            $devices = $devices
                ->join('clients', "clients.id", '=', 'devices.client_id')
                ->where("clients.name", 'like', "%" . $query['client_name'] . "%");
        }

        $devices = $devices->paginate();
        $devices = $devices->appends($request->except('page'));

        return view('devices.index', compact('devices', 'query'));
    }

    /**
     * Показ формы для добавления нового устройства.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Device::class);

        $c_id = $request->query('client_id');
        if ($c_id) {
            $c = Client::find($c_id);

            if ($c) {
                $clients[] = $c;
            }
        }

        return view('devices.create', compact('clients'));
    }

    /**
     * Добавление нового устройства.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Device::class);

        $this->validate($request, [
            'client_id' => 'required',
            'hwid' => 'required',
        ]);

        $device = Device::create($request->all());

        return redirect(route('devices.show', $device->id));
    }

    /**
     * Просмотр устройства
     *
     * @param  \App\Device $device
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        $this->authorize('view', Device::class);

        return view('devices.show', compact('device'));
    }

    /**
     * Показ формы для изменения устройства.
     *
     * @param  \App\Device $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        $this->authorize('update', Device::class);

        $clients = [$device->client];

        return view('devices.edit', compact('device', 'clients'));
    }

    /**
     * Апдейт устройства.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Device $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        $this->authorize('update', Device::class);

        $device->fill($request->all());
        $device->save();

        return redirect(route('devices.show', $device->id));
    }

    /**
     * Удаление устройства.
     *
     * @param  \App\Device $device
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Device $device, Request $request)
    {
        $this->authorize('delete', Device::class);

        $device->delete();

        $redirect_url = $request->query('redirect_uri');
        if ($redirect_url)
            return redirect($redirect_url);

        return redirect()->back();
    }
}

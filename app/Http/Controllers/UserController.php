<?php

namespace App\Http\Controllers;

use App\Group;
use App\Helpers\SettingsStore;
use App\SystemSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Traits\Filter;

/**
 * Класс UsersController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    use Filter;

    /**
     * @var SettingsStore
     */
    protected $settingsStore;

    /**
     * UserController constructor.
     * @param SettingsStore $settingsStore
     */
    function __construct(SettingsStore $settingsStore)
    {
        $this->settingsStore = $settingsStore;
    }

    /**
     * Просмотр всех пользователей.
     *
     * @param $request Request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(Request $request)
    {
        $this->authorize('view', User::class);

        $query = $request->query();
        $users = User::query();
        $users = $this->filter($users, $query);

        $users = $users->paginate();
        $users->appends($request->except('page'));

        $groups = Group::all();

        return view('users.index', compact('users', 'query', 'groups'));
    }

    /**
     * Редактирование пользователя.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function edit(User $user)
    {
        $this->authorize('view_update_form', $user);

        $groups = Group::all();

        return view('users.edit', compact('user', 'groups'));
    }

    /**
     * Апдейт пользователя.
     *
     * Дополнительня фича:
     *  Подтверждение пароля через поле password_confirmation.
     *  Если пароль пустой, то он не апдейтится.
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function update(Request $request, User $user)
    {
        $data = $request->all();

        // Удаление сервисных полей, костыль с чекбоксом
        if (isset($data["is_active"]))
            $data["is_active"] = $request->input('is_active', 'off') == 'on' ? 1 : 0;
        unset($data["_token"]);
        unset($data["_method"]);

        $this->authorize('update', [$user, $data]);

        $data["is_active"] = $request->input('is_active', 'off') == 'on' ? 1 : 0;
        if ($data['password'] == null)
            unset($data['password']);

        $this->validate($request, [
            'password' => 'confirmed'
        ]);

        $user->fill($data);
        $user->save();

        $can_update_users = Auth::user()->group->can_create_update_users == "1";
        return redirect(route($can_update_users ? 'users.index' : 'home'));
    }

    /**
     * Удаление прользователя.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function delete(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();
        return redirect(route('users.index'));
    }

    /**
     * Показ формы для создания пользователя.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create()
    {
        $groups = Group::all();

        $default_group_id = $this->settingsStore->getValue('default_user_group_id');

        return view('users.create', compact('groups', 'default_group_id'));
    }

    /**
     * Добавление нового пользователя.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function store(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
            'name' => 'required',
        ]);

        $data = $request->all();
        $data['api_token'] = str_random(60);

        User::create($data);
        return redirect(route('users.index'));
    }
}

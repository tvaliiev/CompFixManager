<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс Группа.
 *
 * @package App
 * @property integer id Идентификатор группы.
 * @property string name Имя группы.
 * @property Carbon created_at Дата создания группы.
 * @property Carbon updated_at Дата последнего изменения любого из атрибутов группы.
 */
class Group extends Model
{
    /**
     * Поля, которые нельзя массово присвоить.
     *
     * @var array
     */
    protected $guarded = [
      'id', 'created_at', 'updated_at'
    ];

    /**
     * Пользователи, которые принадлежат к группе.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function users(){
        return $this->hasMany(User::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
    protected $fillable = [
        'key', 'value'
    ];

    protected $primaryKey = 'key';
    public $timestamps = false;
    public $incrementing = false;
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * Класс Пользователь.
 *
 * Может быть как клаентом, так и сотрудником.
 *
 * @package App
 * @property integer id Идентификатор пользователя.
 * @property string name Имя пользователя.
 * @property string remember_token Токен для того, чтобы не вводить пароль каждый раз.
 * @property string email Электронная почта пользователя.
 * @property string password Пароль.
 * @property Carbon created_at Дата, когда пользователь был создан.
 * @property Carbon updated_at Дата последнего изменения любого из атрибутов пользователя.
 * @property integer group_id Идентификатор группы, к которой принадлежит пользователь.
 * @property string phone_number Номер телефона пользователя.
 * @property string comment Комментарий к пользователю.
 * @property string allowed_ip IP адреса, с которых разрешено пользователю заходить в систему.
 * @property boolean is_active Является ли пользователь активным (true), либо забанен\уволился\подобное.
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * Массово присваиваемые поля.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group_id', 'is_active', 'phone_number', 'allowed_ip', 'api_token'
    ];

    /**
     * Поля, которые должны быть спрятаны для массивов.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Поля, которые может обновить пользователь сам себе.
     *
     * @var array
     */
    protected $updatable = [
        'password', 'name', 'email', 'phone_number'
    ];

    /**
     * Права (группа), которые определяют возможности пользователя
     * взаимодействовать с системой.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Заказы пользователя.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function orders()
    {
        return $this->hasMany(Order::class, 'client_id');
    }

    /**
     * Устройства пользователя.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function devices()
    {
        return $this->hasMany(Device::class, 'client_id');
    }

    /**
     * @param $val
     */
    function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    /**
     * @return array
     */
    function getUpdatableFields()
    {
        return $this->updatable;
    }

    /**
     * Выбранные (излюбленные) заказы.
     *
     * @return mixed
     */
    function getFavoriteOrdersAttribute()
    {
        return Order::whereIn(
            'id', function ($query) {
            $query->select('favoritable_id')
                ->from((new Favorite)->getTable())
                ->where('user_id', Auth::user()->id)
                ->where('favoritable_type', Order::class);
        })->get();
    }
}

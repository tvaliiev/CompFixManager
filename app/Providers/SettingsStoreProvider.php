<?php

namespace App\Providers;

use App\Helpers\SettingsStore;
use Illuminate\Support\ServiceProvider;

class SettingsStoreProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SettingsStore::class, function($app){
            return new SettingsStore;
        });
    }
}

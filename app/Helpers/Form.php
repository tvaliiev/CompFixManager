<?php

namespace App\Helpers;

/**
 * Class Form
 * @package App\Helpers
 */
class Form
{

    /**
     * Создает компоненты формы, несолько подряд.
     *
     * Доступны следующие:
     *  text, email, password, select, checkbox
     *
     * @param $config
     * @return string
     */
    public static function make($config)
    {
        $return = '';

        foreach ($config as $field_name => $field_data) {
            switch ($type = $field_data['type']) {
                case 'text':
                case 'email':
                case 'password':
                    $return .= Form::input($type, $field_name, $field_data['label'], $field_data['value']);
                    break;
                case 'textarea':
                    $return .= Form::textarea(
                        $field_name,
                        $field_data['label'] ?? null,
                        $field_data['value'] ?? null,
                        $field_data['cols'] ?? null,
                        $field_data['rows'] ?? null,
                        $field_data['hidden'] ?? false
                    );
                    break;
                case 'select':
                    $return .= Form::select(
                        $field_name,
                        $field_data['label'],
                        $field_data['options']['values'],
                        $field_data['options']['selected'] ?? null,
                        $field_data['value_field'],
                        $field_data['name_field'],
                        $field_data['empty_option']
                    );
                    break;
                case 'checkbox':
                    $return .= Form::checkbox($field_name, $field_data['label'], $field_data['checked']);
                    break;
                default:
                    $return .= "<div>No such field type: {$field_data['type']}</div>";
            }

        }

        return $return;
    }

    /**
     * Создает поле указанного типа.
     *
     * @param $type
     * @param $name
     * @param string $label
     * @param string $value
     * @return string
     */
    public static function input($type, $name, $label = '', $value = '')
    {
        return "<div class='form-group'>
            <label for='$name'>$label</label>
            <input type='$type' class='form-control' id='$name' name='$name' value='$value'>
        </div>";
    }

    /**
     * Создает поле с мультистрочным редактированием.
     *
     * @param $name
     * @param string $label
     * @param string $value
     * @param integer|string $cols
     * @param integer|string $rows
     * @return string
     */
    public static function textarea($name, $label = '', $value = '', $cols = 30, $rows = 20, $hidden = false)
    {
        $hidden = $hidden ? "style='display:none;'" : "";
        return "<div class='form-group' $hidden>
            <label for='$name'>$label</label>
            <textarea name='$name' id='$name' cols='$cols' rows='$rows' class='form-control'>$value</textarea>
        </div>";
    }

    /**
     * Создает поле типа select.
     *
     * @param $name
     * @param string $label
     * @param $values
     * @param $selected_value
     * @param string $value_field
     * @param string $name_field
     * @param bool $empty_option
     * @return string
     */
    public static function select(
        $name,
        $label = '',
        $values,
        $selected_value,
        $value_field = 'value',
        $name_field = 'name',
        $empty_option = false
    )
    {
        $empty_option = $empty_option ? "<option value=''>---</option>" : "";
        $return = "<div class='form-group'>
            <label for='$name'>$label</label>
            <select class='form-control' id='$name' name='$name'>
                $empty_option";

        foreach ($values as $option) {
            $value = $option[$value_field];
            $name = $option[$name_field];
            $selected = $value == $selected_value ? "selected='selected'" : "";

            $return .= "<option value='{$value}' $selected>{$name}</option>";
        }

        $return .= "</select></div>";
        return $return;
    }

    /**
     * Создает чекбокс.
     *
     * @param $name
     * @param $label
     * @param bool $checked
     * @return string
     */
    public static function checkbox($name, $label = '', $checked = false)
    {
        $checked = $checked ? "checked='checked'" : "";

        return " <div class='checkbox'>
            <label><input type='checkbox' name='$name' $checked>$label</label>
        </div>";
    }
}
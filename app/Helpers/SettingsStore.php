<?php


namespace App\Helpers;


use App\Group;
use App\StatusOption;
use App\SystemSetting;

/**
 * Класс SettingsStore
 * @package App\Helpers
 */
class SettingsStore
{
    /**
     * Все настройки, которые должны быть возвращены при вызове getAll()
     *
     * @var array
     */
    private $allSettingsList = [
        'default_user_group_id',
        'default_status_option_id'
    ];

    /**
     * Узнать значение по ключу.
     *
     * @param string $key
     * @return string
     */
    public function getValue($key)
    {
        $setting = SystemSetting::find($key);

        if ($setting == null) {
            $setting = SystemSetting::create([
                'key' => $key,
                'value' => $this->getDefaultValue($key)
            ]);
        }

        return $setting->value;
    }

    /**
     * Дефолтные опции для конфига.
     *
     * @param $key string Ключ
     * @return mixed Дефолтное значение
     */
    private function getDefaultValue($key)
    {
        switch ($key) {
            case 'default_user_group_id':
                return Group::first()->id;
                break;
            case 'default_status_option_id':
                return StatusOption::first()->id;
                break;
            default:
                return '';
        }
    }

    /**
     * Установить несколько значений по ключу.
     *
     * Ожидается массив вида:
     * ['key1' => 'value1', 'key2' =>'value2', ...]
     *
     * @param array $arr
     */
    public function setBulk($arr)
    {
        foreach ($arr as $k => $v) {
            $this->setValue($k, $v);
        }
    }

    /**
     * Установить значение по ключу.
     *
     * @param string $key
     * @param string $value
     */
    public function setValue($key, $value)
    {
        $setting = SystemSetting::find($key);

        if ($setting == null) {
            SystemSetting::create([
                'key' => $key,
                'value' => $value
            ]);

            return;
        }

        $setting->value = $value;
        $setting->save();
    }

    /**
     * Узнать все значения.
     *
     * Возвращает массив вида:
     * ['key1' => 'value1', 'key2' =>'value2', ...]
     *
     * @return array
     */
    public function getAll()
    {
        $settings = [];
        foreach (SystemSetting::all() as $setting) {
            $settings[$setting->key] = $setting->value;
        }
        $settings = array_merge($this->getAllDefaultValues(), $settings);

        return $settings;
    }

    /**
     * Узнать все дефолтные опции для конфига.
     *
     * @return array
     */
    private function getAllDefaultValues()
    {
        $settings = [];
        foreach ($this->allSettingsList as $setting) {
            $settings[$setting] = $this->getDefaultValue($setting);
        }

        return $settings;
    }
}